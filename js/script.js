// завдання №1

let userYears = +prompt("Який у Вас вік?");
if (userYears <= 12) {
  alert("Ви ще дитина.");
} else if (userYears < 18) {
  alert("Ви підліток.");
} else {
    alert('Ви дорослий.')
}

// завдання №2

let num = prompt("Введіть число.");
if (!isNaN(num)) {
  alert("Ви ввели число");
} else {
  alert("Ви ввели НЕ число!");
}

// завдання №3

let month = prompt(
  "Який місяць року Вас цікавить? Введіть Україньською мовою, маленькими літерами."
);

switch (month) {
  case 'січень':
  case 'березень':
  case 'травень':
  case 'липень':
  case 'серпень':
  case 'жовтень':
  case 'грудень':
    console.log("В цьому місяці 31 днів.");
    break;
  case 'квітень':
  case 'червень':
  case 'вересень':
  case 'листопад':
    console.log("В цьому місяці 30 днів.");
  case 'лютий':
    console.log("В цьому місяці 28 чи 29 днів, залежить від високосного року.");
    break;
}


